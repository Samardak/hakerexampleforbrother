﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgregatorController : Controller
    {

        [Route("TakeIp")]
        public void TakeIp(string ipData)
        {
            Console.WriteLine("___________________________________________________");
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(ipData);
            Console.WriteLine("***************************************************");
        }

    }
}