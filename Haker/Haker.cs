﻿using System;
using System.Net.Http;

namespace HakerNamespace
{
    public class Haker
    {
        #region field
        private Uri uri;
        private HttpClient client;
        #endregion field

        #region ctor
        public Haker(string url)
        {
            uri = new Uri(url);
            client = new HttpClient();
        }
        #endregion ctor

        #region method(s)
        public string GetIpAdress()
        {
            var response = client.GetStringAsync(uri).Result;
            return response;
        }
        #endregion method(s)

        public void SendIpToAgregator(string ip)
        {
            var uriAgregator = "http://localhost:5000/api/Agregator/TakeIp?ipData=" + ip;
            var result = client.GetStringAsync(uriAgregator).Result;
        }
    }
}
